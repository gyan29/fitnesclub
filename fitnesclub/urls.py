
from django.contrib import admin
from django.urls import path,include

from rest_framework_simplejwt.views import TokenObtainPairView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('user/',  include('userinfo.urls')),
    path('apiadmin/',  include('admin_api.urls')),
    path('login/', TokenObtainPairView.as_view(), name='token_obtain_pair'),

]
