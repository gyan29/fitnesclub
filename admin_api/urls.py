from django.urls import path
from django.urls import path
from .views import *



urlpatterns = [
   
    path('dietuserlist/', DietUserTypeList.as_view()),
    path('typeofdietlist/', TypeOfDietList.as_view()),
    path('calorierecommended/', CalorieRecommendedList.as_view()),
    path('medicalconditions/', MedicalConditionsList.as_view()),
    path('foodintolerance/', FoodIntoleranceList.as_view()),
    path('dietaryhabits/', DietaryHabitsList.as_view()),
    path('mealtime/', MealTimeList.as_view()),
    path('foodswap/', FoodSwapList.as_view()),
    path('healthlabel/', HealthLabelList.as_view()),
    path('foodregion/', FoodRegionList.as_view()),
    path('dietlabels/', DietLabelsList.as_view()),
    path('foodunit/', FoodUnitList.as_view()),
    path('recipesfood/', RecipesFoodList.as_view()),

    path('datafood/', DataFoodList.as_view()),

    path('createdietplan/', CreateDietplanList.as_view()),
    path('diet_template/', DietTemplateList.as_view()),
    path('diet_single_user/<str:user_id>/',DietplanDetail.as_view()),


    path('read_file/',FoodDataList.as_view()),
    path('get_file_data/<str:user_id>/',FoodDataDetail.as_view()),

   
]


  









    