# Generated by Django 2.2.19 on 2021-04-26 12:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('admin_api', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fooddata',
            name='digest',
        ),
        migrations.RemoveField(
            model_name='fooddata',
            name='ingredientLines',
        ),
        migrations.RemoveField(
            model_name='fooddata',
            name='totalDaily',
        ),
        migrations.RemoveField(
            model_name='fooddata',
            name='totalNutrients',
        ),
    ]
