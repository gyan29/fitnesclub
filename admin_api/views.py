from datetime import date
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from django.shortcuts import render
from django.http import HttpResponse
import json, requests,os, urllib
from .serializers import *
from .models import *
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36', "Upgrade-Insecure-Requests": "1","DNT": "1","Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8","Accept-Language": "en-US,en;q=0.5","Accept-Encoding": "gzip, deflate"}


def validate_id(id):
    if len(id) != 24:
        return False
    return True



#######################################################################################################################

# Create and All data get 
class CreateDietplanList(APIView):
    @staticmethod
    def get(request):
        queryset = CreateDietplan.objects.all()
        serializer_data = CreateDietplanListSerializer(queryset, many=True)
        return Response(serializer_data.data)

    @staticmethod
    def post(request):
        try:
            serializer = CreateDietplanListSerializer(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()
                return Response({"Message": "Dietplan Update  Data Succesfully"},status=status.HTTP_201_CREATED)

        except CreateDietplan.DoesNotExist:
            return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_400_BAD_REQUEST)



# Get Single User and Update ID
class DietplanDetail(APIView):
    @staticmethod
    def get(request, user_id):
        serializer = CreateDietplanListSerializer(data=request.data)
        try:
            obj = CreateDietplan.objects.get(_id=user_id)
            serializer = CreateDietplanListSerializer(obj)
            return Response(serializer.data)
        except CreateDietplan.DoesNotExist:
            return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def put(request, user_id):
        try:
            if validate_id(user_id):
                obj = CreateDietplan.objects.get(_id=user_id)
                serializer = CreateDietplanListSerializer(obj, data=request.data)
                if serializer.is_valid(raise_exception=True):
                    serializer.save()
                    return Response({"Message": "Dietplan Update  Data Succesfully"},status=status.HTTP_201_CREATED )
            return Response({"Error": "Invalid user id"}, status=status.HTTP_400_BAD_REQUEST)

        except obj.DoesNotExist:
            return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_400_BAD_REQUEST)

#######################################################################################################################

class DietTemplateList(APIView):
    @staticmethod
    def get(request):
        queryset = DietTemplate.objects.all()
        serializer_data = ShowDietTemplateListSerializer(queryset, many=True)
        return Response(serializer_data.data)
  
    @staticmethod
    def post(request):
        serializer = DietTemplateListSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"Message": "Create DietTemplate Data Succesfully"}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#######################################################################################################################



# TypeOfDiet 
class RecipesFoodList(APIView):
    @staticmethod
    def get(request):
        queryset = RecipesFood.objects.all()
        serializer_data = RecipesFoodListSerializer(queryset, many=True)
        return Response(serializer_data.data)


    @staticmethod
    def post(request):
        try:
            url = requests.get("http://13.232.172.131/api/user/recipes",headers=headers)
            # breakpoint()
            response = url.json()
           
            if url.status_code == 200:
               
                obj_list = RecipesFood.objects.all()
                if obj_list.exists():
                    obj_list.delete()
                    for jsondata in response:
                        updatejson = RecipesFood.objects.create(**jsondata)
                    return Response({"Message": "RecipesFoodList Data Succesfully"}, status=status.HTTP_200_OK)
                else:
                    breakpoint()
                    for jsondata in response:
                        updatejson = RecipesFood.objects.create(**jsondata)
                    return Response({"Message": "RecipesFoodList Data Update Succesfully"}, status=status.HTTP_200_OK)
                    # return HttpResponse('no')
        except Exception as e:
            return Response({"Error Url Not Found":  e}, status=status.HTTP_404_NOT_FOUND)



######################################################################################################################################


import pandas as pd
File_path = r'C:\\Users\\pcpc\\Desktop\\second.json'
id_value = 1
 # obj_list = FoodData.objects.all().delete()

class FoodDataList(APIView):
    @staticmethod
    def get(request):
        queryset = FoodData.objects.all()
        serializer_data = FoodDataListSerializer(queryset, many=True)
        return Response(serializer_data.data)

    @staticmethod
    def post(request):
        try:
            df = pd.read_json (File_path)
            df2  = df.drop(['q', 'from', 'to', 'more', 'count'], axis=1)
            marks_list = df2['hits'].tolist()
            dfnew = pd.DataFrame(marks_list)
            dfupdate  = dfnew.drop(['bookmarked', 'bought'], axis=1)
            json_str = dfupdate.to_json()
            type(json_str)
            excel_json = json.loads(json_str)
            for jsondata in excel_json.values():
                for data in jsondata.values():
                    data.pop('ingredientLines'),data.pop('totalNutrients'),data.pop('digest'),data.pop('totalDaily')
                    data["yields"] = data.pop("yield")
                    data['id'] = id_value 
                    data['meeltime'] = 1 
                    calories = data["calories"]
                    print("calories", calories)
                    # breakpoint()
                    if FoodData.objects.filter(calories = calories) or FoodData.objects.filter(id=id_value) :
                        return Response({"Message": "Id is allready Exists"}, status=status.HTTP_200_OK) 
                    else:
                        updatejson = FoodData.objects.create(**data)
                        return Response({"Message": "RecipesFoodList Data Succesfully"}, status=status.HTTP_200_OK)    

        except Exception as e:
            return Response({"Error Url Not Found" : e}, status=status.HTTP_404_NOT_FOUND)


# calories

##########################################################################################



# UserTypeList
class DietUserTypeList(APIView):
    @staticmethod
    def get(request):
        queryset = DietUserType.objects.all()
        serializer_data = DietUserTypeListSerializer(queryset, many=True)
        return Response(serializer_data.data)


    @staticmethod
    def post(request):
        try:
            url = requests.get("http://13.232.172.131/api/user/diet_user_type",headers=headers)
            response = url.json()
            if url.status_code == 200:
                obj_list = DietUserType.objects.all()
                if obj_list:
                    obj_list.delete()
                    for jsondata in response:
                        updatejson = DietUserType.objects.create(**jsondata)
                    return Response({"Message": "DietUserType Data Succesfully"}, status=status.HTTP_200_OK)
                else:
                    for jsondata in response:
                        updatejson = DietUserType.objects.create(**jsondata)
                    return Response({"Message": "DietUserType Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error Url Not Found":  url.status_code}, status=status.HTTP_404_NOT_FOUND)


##########

# TypeOfDiet 
class TypeOfDietList(APIView):
    @staticmethod
    def get(request):
        queryset = TypeOfDiet.objects.all()
        serializer_data = TypeOfDietListSerializer(queryset, many=True)
        return Response(serializer_data.data)


    @staticmethod
    def post(request):
        try:
            url = requests.get("http://13.232.172.131/api/user/diet_type_of_diet",headers=headers)
            response = url.json()
            if url.status_code == 200:
                obj_list = TypeOfDiet.objects.all()
                if obj_list:
                    obj_list.delete()
                    for jsondata in response:
                        updatejson = TypeOfDiet.objects.create(**jsondata)
                    return Response({"Message": "TypeOfDiet Data Succesfully"}, status=status.HTTP_200_OK)
                else:
                    for jsondata in response:
                        updatejson = TypeOfDiet.objects.create(**jsondata)
                    return Response({"Message": "TypeOfDiet Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error Url Not Found":  url.status_code}, status=status.HTTP_404_NOT_FOUND)



# CalorieRecommended
class CalorieRecommendedList(APIView):
    @staticmethod
    def get(request):
        queryset = CalorieRecommended.objects.all()
        serializer_data = CalorieRecommendedListSerializer(queryset, many=True)
        return Response(serializer_data.data)

    @staticmethod
    def post(request):
        try:
            url = requests.get("http://13.232.172.131/api/user/diet_calorie_recommended",headers=headers)
            response = url.json()
            if url.status_code == 200:
                obj_list = CalorieRecommended.objects.all()
                if obj_list:
                    obj_list.delete()
                    for jsondata in response:
                        updatejson = CalorieRecommended.objects.create(**jsondata)
                    return Response({"Message": "CalorieRecommended Data Succesfully"}, status=status.HTTP_200_OK)
                else:
                    for jsondata in response:
                        updatejson = CalorieRecommended.objects.create(**jsondata)
                    return Response({"Message": "CalorieRecommended Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error Url Not Found":  url.status_code}, status=status.HTTP_404_NOT_FOUND)





# MedicalConditions
class MedicalConditionsList(APIView):
    @staticmethod
    def get(request):
        queryset = MedicalConditions.objects.all()
        serializer_data = MedicalConditionsListSerializer(queryset, many=True)
        return Response(serializer_data.data)



    @staticmethod
    def post(request):
        try:
            url = requests.get("http://13.232.172.131/api/user/diet_medical_conditions",headers=headers)
            response = url.json()
            if url.status_code == 200:
                obj_list = MedicalConditions.objects.all()
                if obj_list:
                    obj_list.delete()
                    for jsondata in response:
                        updatejson = MedicalConditions.objects.create(**jsondata)
                    return Response({"Message": "MedicalConditions Data Succesfully"}, status=status.HTTP_200_OK)
                else:
                    for jsondata in response:
                        updatejson = MedicalConditions.objects.create(**jsondata)
                    return Response({"Message": "MedicalConditions Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error Url Not Found":  url.status_code}, status=status.HTTP_404_NOT_FOUND)



# FoodIntolerance
class FoodIntoleranceList(APIView):
    @staticmethod
    def get(request):
        queryset = FoodIntolerance.objects.all()
        serializer_data = FoodIntoleranceListSerializer(queryset, many=True)
        return Response(serializer_data.data)


    @staticmethod
    def post(request):
        try:
            # breakpoint()
            url = requests.get("http://13.232.172.131/api/user/diet_food_intolerance",headers=headers)
            response = url.json()
            if url.status_code == 200:
                obj_list = FoodIntolerance.objects.all()
                if obj_list:
                    obj_list.delete()
                    for jsondata in response:
                        updatejson = FoodIntolerance.objects.create(**jsondata)
                    return Response({"Message": "DietaryHabits Data Succesfully"}, status=status.HTTP_200_OK)
                else:
                    for jsondata in response:
                        updatejson = FoodIntolerance.objects.create(**jsondata)
                    return Response({"Message": "DietaryHabits Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error Url Not Found":  url.status_code}, status=status.HTTP_404_NOT_FOUND)




# DietaryHabits
class DietaryHabitsList(APIView):
    @staticmethod
    def get(request):
        queryset = DietaryHabits.objects.all()
        serializer_data = DietaryHabitsListSerializer(queryset, many=True)
        return Response(serializer_data.data)


    @staticmethod
    def post(request):
        try:
            # breakpoint()
            url = requests.get("http://13.232.172.131/api/user/diet_dietary_habits",headers=headers)
            response = url.json()
            if url.status_code == 200:
                obj_list = DietaryHabits.objects.all()
                if obj_list:
                    obj_list.delete()
                    for jsondata in response:
                        updatejson = DietaryHabits.objects.create(**jsondata)
                    return Response({"Message": "DietaryHabits Data Succesfully"}, status=status.HTTP_200_OK)
                else:
                    for jsondata in response:
                        updatejson = DietaryHabits.objects.create(**jsondata)
                    return Response({"Message": "DietaryHabits Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error Url Not Found":  url.status_code}, status=status.HTTP_404_NOT_FOUND)


    

    

# MealTime
class MealTimeList(APIView):
    @staticmethod
    def get(request):
        queryset = MealTime.objects.all()
        serializer_data = MealTimeListSerializer(queryset, many=True)
        return Response(serializer_data.data)


    @staticmethod
    def post(request):
        try:
            url = requests.get("http://13.232.172.131/api/user/meal_time",headers=headers)
            response = url.json()
            if url.status_code == 200:
                obj_list = MealTime.objects.all()
                if obj_list:
                    obj_list.delete()
                    for jsondata in response:
                        updatejson = MealTime.objects.create(**jsondata)
                    return Response({"Message": "MealTime Data Succesfully"}, status=status.HTTP_200_OK)
                else:
                    for jsondata in response:
                        updatejson = MealTime.objects.create(**jsondata)
                    return Response({"Message": "MealTime Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error Url Not Found":  url.status_code}, status=status.HTTP_404_NOT_FOUND)


# FoodSwap
class FoodSwapList(APIView):
    @staticmethod
    def get(request):
        queryset = FoodSwap.objects.all()
        serializer_data = FoodSwapListSerializer(queryset, many=True)
        return Response(serializer_data.data)

    @staticmethod
    def post(request):
        try:
            url = requests.get("http://13.232.172.131/api/user/foodswap",headers=headers)
            response = url.json()
            if url.status_code == 200:
                obj_list = FoodSwap.objects.all()
                if obj_list:
                    obj_list.delete()
                    for jsondata in response:
                        updatejson = FoodSwap.objects.create(**jsondata)
                    return Response({"Message": "FoodSwap Data Succesfully"}, status=status.HTTP_200_OK)
                else:
                    for jsondata in response:
                        updatejson = FoodSwap.objects.create(**jsondata)
                    return Response({"Message": "FoodSwap Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error Url Not Found":  url.status_code}, status=status.HTTP_404_NOT_FOUND)




    
# HealthLabel
class HealthLabelList(APIView):
    @staticmethod
    def get(request):
        queryset = HealthLabel.objects.all()
        serializer_data = HealthLabelListSerializer(queryset, many=True)
        return Response(serializer_data.data)


    @staticmethod
    def post(request):
        try:
            url = requests.get("http://13.232.172.131/api/user/health_label",headers=headers)
            response = url.json()
            if url.status_code == 200:
                obj_list = HealthLabel.objects.all()
                if obj_list:
                    obj_list.delete()
                    for jsondata in response:
                        updatejson = HealthLabel.objects.create(**jsondata)
                    return Response({"Message": "HealthLabel Data Succesfully"}, status=status.HTTP_200_OK)
                else:
                    for jsondata in response:
                        updatejson = HealthLabel.objects.create(**jsondata)
                    return Response({"Message": "HealthLabel Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error Url Not Found":  url.status_code}, status=status.HTTP_404_NOT_FOUND)


# FoodRegion
class FoodRegionList(APIView):
    @staticmethod
    def get(request):
        queryset = FoodRegion.objects.all()
        serializer_data = FoodRegionListSerializer(queryset, many=True)
        return Response(serializer_data.data)

    @staticmethod
    def post(request):
        try:
            url = requests.get("http://13.232.172.131/api/user/foodregion",headers=headers)
            response = url.json()
            if url.status_code == 200:
                obj_list = FoodRegion.objects.all()
                if obj_list:
                    obj_list.delete()
                    for jsondata in response:
                        updatejson = FoodRegion.objects.create(**jsondata)
                    return Response({"Message": "FoodRegion Data Succesfully"}, status=status.HTTP_200_OK)
                else:
                    for jsondata in response:
                        updatejson = FoodRegion.objects.create(**jsondata)
                    return Response({"Message": "FoodRegion Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error Url Not Found":  url.status_code}, status=status.HTTP_404_NOT_FOUND)


# DietLabels
class DietLabelsList(APIView):
    @staticmethod
    def get(request):
        queryset = DietLabels.objects.all()
        serializer_data = DietLabelsListSerializer(queryset, many=True)
        return Response(serializer_data.data)

    @staticmethod
    def post(request):
        try:
            url = requests.get("http://13.232.172.131/api/user/dietlabels",headers=headers)
            response = url.json()
            if url.status_code == 200:
                obj_list = DietLabels.objects.all()
                if obj_list:
                    obj_list.delete()
                    for jsondata in response:
                        updatejson = DietLabels.objects.create(**jsondata)
                    return Response({"Message": "DietLabels Data Succesfully"}, status=status.HTTP_200_OK)
                else:
                    for jsondata in response:
                        updatejson = DietLabels.objects.create(**jsondata)
                    return Response({"Message": "DietLabels Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error Url Not Found":  url.status_code}, status=status.HTTP_404_NOT_FOUND)



# FoodUnit
class FoodUnitList(APIView):
    @staticmethod
    def get(request):
        queryset = FoodUnit.objects.all()
        serializer_data = FoodUnitListSerializer(queryset, many=True)
        return Response(serializer_data.data)

    @staticmethod
    def post(request):
        try:
            url = requests.get("http://13.232.172.131/api/user/food_unit",headers=headers)
            response = url.json()
            if url.status_code == 200:
                obj_list = FoodUnit.objects.all()
                if obj_list:
                    obj_list.delete()
                    for jsondata in response:
                        # print(jsondata)
                        updatejson = FoodUnit.objects.create(**jsondata)
                        pass
                    return Response({"Message": "FoodUnit Data Succesfully"}, status=status.HTTP_200_OK)
                else:
                    for jsondata in response:
                        # print(jsondata)
                        updatejson = FoodUnit.objects.create(**jsondata)
                        pass
                    return Response({"Message": "FoodUnit Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error Url Not Found":  url.status_code}, status=status.HTTP_404_NOT_FOUND)





#################################################################################################################

class FoodDataDetail(APIView):
    @staticmethod
    def get(request, user_id):
        serializer = FoodDataListSerializer(data=request.data)
        try:
            obj = FoodData.objects.get(_id=user_id)
            serializer = FoodDataListSerializer(obj)
            return Response(serializer.data)
        except FoodData.DoesNotExist:
            return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_400_BAD_REQUEST)



    @staticmethod
    def put(request, user_id):
        try:
            if validate_id(user_id):
                obj = FoodData.objects.get(_id=user_id)
                serializer = FoodDataListSerializer(obj, data=request.data)
                if serializer.is_valid(raise_exception=True):
                    serializer.save()
                    return Response({"Message": "Food Data Update  Data Succesfully"},status=status.HTTP_201_CREATED )
            return Response({"Error": "Invalid user id"}, status=status.HTTP_400_BAD_REQUEST)

        except obj.DoesNotExist:
            return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_400_BAD_REQUEST)

















    