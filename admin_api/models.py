from bson.objectid import ObjectId
from jsonfield import JSONField
from djongo import models
import uuid, urllib, os



# http://13.232.172.131/api/user/diet_user_type
class DietUserType(models.Model):
    name = models.CharField(max_length=50)
    status = models.CharField(max_length=50)
   
# http://13.232.172.131/api/user/diet_type_of_diet
class TypeOfDiet(models.Model):
    name = models.CharField(max_length=50)
    macros_bifurcation = models.CharField(max_length=100)
    usertype_id = models.CharField(max_length=50)

# http://13.232.172.131/api/user/diet_calorie_recommended
class CalorieRecommended(models.Model):
    name = models.CharField(max_length=50)
    usertype_id = models.CharField(max_length=50)

# http://13.232.172.131/api/user/diet_medical_conditions
class MedicalConditions(models.Model):
    name = models.CharField(max_length=50)
    usertype_id = models.CharField(max_length=50)

# http://13.232.172.131/api/user/diet_food_intolerance
class FoodIntolerance(models.Model):
    name = models.CharField(max_length=50)
    usertype_id = models.CharField(max_length=50)

# http://13.232.172.131/api/user/diet_dietary_habits
class DietaryHabits(models.Model):
    name = models.CharField(max_length=50)
    usertype_id = models.CharField(max_length=50)

# http://13.232.172.131/api/user/meal_time
class MealTime(models.Model):
    name = models.CharField(max_length=50)
    food_time = models.CharField(max_length=50)
    status = models.CharField(max_length=50)
 
# http://13.232.172.131/api/user/foodswap
class FoodSwap(models.Model):
    name = models.CharField(max_length=50)
    foodtime_id = models.CharField(max_length=50)
   
# http://13.232.172.131/api/user/health_label
class HealthLabel(models.Model):
    name = models.CharField(max_length=50)
    labelname = models.CharField(max_length=50)
    status = models.CharField(max_length=50)
   
# http://13.232.172.131/api/user/foodregion
class FoodRegion(models.Model):
    name = models.CharField(max_length=50)
    status = models.CharField(max_length=50)
    
# http://13.232.172.131/api/user/dietlabels
class DietLabels(models.Model):
    name = models.CharField(max_length=50)
    labelname = models.CharField(max_length=50)
    status = models.CharField(max_length=50)
    
# http://13.232.172.131/api/user/food_unit
class FoodUnit(models.Model):
    quantity = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    measure = models.CharField(max_length=50)
    unit = models.CharField(max_length=50)
    status = models.CharField(max_length=50)

# DietPlan
class CreateDietplan(models.Model):
    _id = models.ObjectIdField()
    id = models.UUIDField(default=uuid.uuid4,editable=False,primary_key=False)
    usertype = models.CharField(max_length=50)
    calories = models.CharField(max_length=50)
    typeofdiet = models.CharField(max_length=50)
    macros_bifurcation = models.CharField(max_length=100)
    dietary_habits = models.CharField(max_length=100)
    medical_condition = models.ListField(blank=True, null=True)
    food_intolrance = models.ListField(blank=True, null=True)
    no_of_meal = models.ListField(blank=True, null=True)

    objects = models.DjongoManager()


    
# Diet Template
class DietTemplate(models.Model):
    _id = models.ObjectIdField()
    id = models.UUIDField(default=uuid.uuid4,editable=False,primary_key=False)
    foodname = models.CharField(max_length=100)
    yeild = models.IntegerField()
    foodtime = models.IntegerField()
    foodswap = models.IntegerField()
    healthlabels = models.CharField(max_length=200)
    dietlabels = models.CharField(max_length=200)
    foodregion = models.CharField(max_length=200)
    food_preparation_time = models.IntegerField()
    cooked_food = models.IntegerField()
    intake_food = models.IntegerField()
    protein = models.IntegerField()
    cho = models.IntegerField()
    fiber = models.IntegerField()
    about = models.IntegerField()
    benifit = models.IntegerField()
    cook_food_measurement = models.IntegerField()
    intake_food_quantity = models.IntegerField()
    food_preparation_method = models.IntegerField()
    calories = models.FloatField()
    weight = models.FloatField()
    ingredients_details = models.ListField()
   
    objects = models.DjongoManager()



# CSV File
class RecipesFood(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to=None, height_field=None, width_field=None, max_length=None)
    yeild = models.CharField(max_length=50)
    calories = models.CharField(max_length=50)
    totalweight = models.CharField(max_length=50)
    foodtime = models.CharField(max_length=50)
    status = models.CharField(max_length=50)
    recipe_cautions = models.ListField()
    recipe_dietlabels = models.ListField()
    recipe_healthlabels = models.ListField()
    recipe_ingredients = models.ListField()
    recipe_region = JSONField()

    objects = models.DjongoManager()



class FoodData(models.Model):
    _id = models.ObjectIdField()
    id = models.PositiveIntegerField(primary_key=False)  #  ,primary_key=False)
    uri = models.CharField(max_length=200)
    label = models.CharField(max_length=100)
    image = models.ImageField(upload_to=None, height_field=None, width_field=None, max_length=None)
    source = models.CharField(max_length=100)
    url = models.CharField(max_length=200)
    shareAs = models.CharField(max_length=200)
    yields = models.CharField(max_length=50)
    calories = models.CharField(max_length=50)
    totalWeight = models.CharField(max_length=50)
    dietLabels =  models.ListField()
    healthLabels =  models.ListField()
    cautions =  models.ListField()
    ingredients =  models.ListField()
    meeltime =  models.IntegerField()

    objects = models.DjongoManager()





