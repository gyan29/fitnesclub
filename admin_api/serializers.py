import re
from rest_framework import serializers, status
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from .models import *

##
class DietUserTypeListSerializer(serializers.ModelSerializer):
    class Meta:
        model = DietUserType
        fields = '__all__'

##
class TypeOfDietListSerializer(serializers.ModelSerializer):
    class Meta:
        model = TypeOfDiet
        fields = '__all__'

##
class CalorieRecommendedListSerializer(serializers.ModelSerializer):
    class Meta:
        model = CalorieRecommended
        fields = '__all__'

##
class MedicalConditionsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicalConditions
        fields = '__all__'

##
class FoodIntoleranceListSerializer(serializers.ModelSerializer):
    class Meta:
        model = FoodIntolerance
        fields = '__all__'

##
class DietaryHabitsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = DietaryHabits
        fields = '__all__'

##
class MealTimeListSerializer(serializers.ModelSerializer):
    class Meta:
        model = MealTime
        fields = '__all__'

###
class FoodSwapListSerializer(serializers.ModelSerializer):
    class Meta:
        model = FoodSwap
        fields = '__all__'

###
class HealthLabelListSerializer(serializers.ModelSerializer):
    class Meta:
        model = HealthLabel
        fields = '__all__'

###
class FoodRegionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = FoodRegion
        fields = '__all__'

##
class DietLabelsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = DietLabels
        fields = '__all__'

##
class FoodUnitListSerializer(serializers.ModelSerializer):
    class Meta:
        model = FoodUnit
        fields = '__all__'


###
class CreateDietplanListSerializer(serializers.ModelSerializer):
    _id = serializers.CharField(required=False)
   
    class Meta:
        model = CreateDietplan
        exclude = (["id"])
        # fields = "__all__"


class DietTemplateListSerializer(serializers.ModelSerializer):
    user_diet_template_id = serializers.CharField(required=False)
    class Meta:
        model = DietTemplate
        # fields = ["_id"]
        fields = "__all__"


###
class ShowDietTemplateListSerializer(serializers.ModelSerializer):
    _id = serializers.CharField(required=False)
    
    # mealtime = MealTimeListSerializer(many=True)

    class Meta:
        model = DietTemplate
        fields = (["_id","foodname","yeild" ,"foodtime" ,"foodtime" ,"foodswap" ,"healthlabels","dietlabels","foodregion","food_preparation_time",
        "cooked_food","intake_food","protein","cho","fiber","about","benifit","cook_food_measurement","intake_food_quantity","food_preparation_method",
        "calories","weight","ingredients_details"])
        


    ingredients_details = serializers.SerializerMethodField()
    def get_ingredients_details(self, obj):
        return obj.ingredients_details



class FoodDataListSerializer(serializers.ModelSerializer):
    _id = serializers.CharField(required=False)
    image = serializers.ImageField(required=False   )
    # mealtime = MealTimeListSerializer(read_only=True)
    # mealtime = serializers.SerializerMethodField()
    #  level = LevelSerializer(read_only=True)

    class Meta:
        model = FoodData
        # fields = '__all__'
        fields = ["_id","id","meeltime", "uri" , "label","image","source","url","shareAs" ,"yields" , "calories" ,
        "totalWeight","dietLabels" ,"healthLabels","cautions" ,"ingredientLines" ,"ingredients" 
        ,"totalNutrients" ,"totalDaily", "digest" ]


    dietLabels = serializers.SerializerMethodField()
    def get_dietLabels(self, obj):
        return obj.dietLabels
    
    healthLabels = serializers.SerializerMethodField()
    def get_healthLabels(self, obj):
        return obj.healthLabels
   
    cautions = serializers.SerializerMethodField()
    def get_cautions(self, obj):
        return obj.cautions
   
    ingredientLines = serializers.SerializerMethodField()
    def get_ingredientLines(self, obj):
        return obj.ingredientLines
   
    ingredients = serializers.SerializerMethodField()
    def get_ingredients(self, obj):
        return obj.ingredients
   
    digest = serializers.SerializerMethodField()
    def get_digest(self, obj):
        return obj.digest


    # breakFast = serializers.SerializerMethodField()
    # def get_breakFast(self, obj):
    #     return obj.breakFast
   
    # mealtime = serializers.SerializerMethodField()
    # def get_mealtime(self, obj):
    #    data = MealTimeListSerializer(MealTime.obj.all(), many=True).data
    #    return data
 
  

















    



    



    
 