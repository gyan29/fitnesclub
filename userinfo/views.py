from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from django.shortcuts import render
from django.http import HttpResponse
import json, requests,random, os, urllib
from datetime import date
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.permissions import (AllowAny,IsAuthenticated)
from django.contrib.auth.hashers import make_password
from .serializers import *
from .models import *
from .calculation import *
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36', "Upgrade-Insecure-Requests": "1","DNT": "1","Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8","Accept-Language": "en-US,en;q=0.5","Accept-Encoding": "gzip, deflate"}



def random_string():
    return str(random.randint(1000, 9999))


def validate_id(id):
    if len(id) != 24:
        return False
    return True

##
class LoginView(TokenObtainPairView):
    serializer_class = LoginSerializer


##################################################################################################################

class RegisterUserList(APIView):
    @staticmethod
    def post(request):
        serializers = RegisterUserListSerializer(data=request.data)
        
        if serializers.is_valid(raise_exception=True):
            random=random_string()
            password = make_password(random)
            if RegisterUser.objects.filter(phone_number=serializers.validated_data['phone_number']).last():
                return Response({"Error": "phone_number already exists"}, status=status.HTTP_400_BAD_REQUEST)
            obj = RegisterUser.objects.create(country_code=serializers.validated_data['country_code'],phone_number=serializers.validated_data['phone_number'],
             password=password,device_id=serializers.validated_data['device_id'])
            return Response({'message': 'phone_numberr is registered', "userId": str(obj._id), "otp": random},status=status.HTTP_201_CREATED)
########################################################################################################################

class UserDetailList(APIView):
   
    permission_classes = (IsAuthenticated,)
    @staticmethod
    def post(request,user_id):
        serializer = UserDetaillistSerializer(data=request.data)
        if validate_id(user_id):
            user_obj = RegisterUser.objects.get(_id=user_id)
            if serializer.is_valid(raise_exception=True):
                try:
                    UserDetail.objects.create(user=user_obj,name=serializer.validated_data["name"],
                    gender=serializer.validated_data["gender"],age=serializer.validated_data["age"],
                    waist=serializer.validated_data["waist"],
                    current_weight=serializer.validated_data["current_weight"],height=serializer.validated_data["height"],
                    target_weight=serializer.validated_data["target_weight"],diffcult_level=serializer.validated_data["diffcult_level"],
                    fitness_goals=serializer.validated_data["fitness_goals"],activity=serializer.validated_data["activity"],
                    height_unit=serializer.validated_data["height_unit"],weight_unit=serializer.validated_data["weight_unit"],)
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                except Exception as e:
                    return Response({"Error": f"User Data is Allready exists, Post Request Is Not Allowed"}, status=status.HTTP_404_NOT_FOUND)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)        
        else: 
            return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_404_NOT_FOUND)


    @staticmethod
    def patch(request, user_id):
        try:
            if validate_id(user_id):
                obj = UserDetail.objects.get(user_id=user_id)
                serializer = UserDetaillistSerializer(obj, data=request.data)
                if serializer.is_valid(raise_exception=True):
                    serializer.save()
                    return Response({"Message": "UserDetail Update  Data Succesfully"},status=status.HTTP_201_CREATED )
            return Response({"Error": "Invalid user id"}, status=status.HTTP_400_BAD_REQUEST)

        except UserDetail.DoesNotExist:
            return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_400_BAD_REQUEST)



    @staticmethod
    def get(request, user_id):
        try:
            if validate_id(user_id):
                obj = UserDetail.objects.get(user_id=user_id)
                serializer = UserDetaillistSerializer(obj)  
                return Response(serializer.data)
            return Response({"Error": "Invalid user id"}, status=status.HTTP_400_BAD_REQUEST)
        except UserDetail.DoesNotExist:
            return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_404_NOT_FOUND)

#######################################################################################################################


class HealthCalculationList(APIView):

    permission_classes = (IsAuthenticated,)
    @staticmethod
    def post(request, user_id):
        try:
            if validate_id(user_id):
                user = RegisterUser.objects.get(_id=user_id)
                user_obj = UserDetail.objects.get(user_id=user)

                if HealthCalculation.objects.filter(user=user_obj).last():
                    return Response({"Error": f"User id is allready {user_id} exists"},status=status.HTTP_404_NOT_FOUND)
                   
                detail = UserDetaillistSerializer(user_obj).data
                bmi = BMI(detail['current_weight'], detail["height"])
                ibw = IBW(detail["height"])
                rmr = RMR(detail['current_weight'], detail["height"], detail["age"], detail["gender"])
                cal = req_cal(rmr, detail["activity"], detail["fitness_goals"], detail["diffcult_level"],detail["gender"])
                body_water = TBW(detail["height"], detail['current_weight'])
                glass = water_intake(detail['current_weight'])
                bd_fat = body_fat(detail['current_weight'], detail["waist"], detail["gender"])
                mus_fat = muscle_fat(bd_fat, detail['current_weight'])
        
                obj = HealthCalculation.objects.create(user=user_obj, total_body_water=body_water, bmi=bmi, bmr=rmr, ibw=ibw,
                                            water=glass, your_target=user_obj.target_weight, calorie=cal, fat=bd_fat,
                                            muscles_fat=mus_fat)
                return Response({"data": "data is insert"}, status=status.HTTP_200_OK)
            return Response({"Error": "Invalid user id"}, status=status.HTTP_400_BAD_REQUEST)
        
        except RegisterUser.DoesNotExist:
            return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_404_NOT_FOUND)


    @staticmethod
    def get(request, user_id):
        try:
            if validate_id(user_id):
                user_obj = HealthCalculation.objects.get(user_id=user_id)
                serializer = HealthCalculationListSerializer(user_obj)
                return Response(serializer.data)
            return Response({"Error": "Invalid user id"}, status=status.HTTP_400_BAD_REQUEST)

        except HealthCalculation.DoesNotExist:
            return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_404_NOT_FOUND)
#######################################################################################################################


# height Measurment data
class HeightList(APIView):

    @staticmethod
    def get(request):
        queryset = Height.objects.all()
        serializer_data = HeightListSerializer(queryset, many=True)
        return Response(serializer_data.data)


    @staticmethod
    def post(request):
        try:
            url = requests.get("http://13.232.172.131/api/user/height_mesurement",headers=headers)
            response = url.json()
            if url.status_code == 200:
                obj_list = Height.objects.all()
                if obj_list:
                    obj_list.delete()
                    for jsondata in response:
                        updatejson = Height.objects.create(**jsondata)
                    return Response({"Message": "Height Data Succesfully"}, status=status.HTTP_200_OK)
                else:
                    for jsondata in response:
                        updatejson = DietUserType.objects.create(**jsondata)
                    return Response({"Message": "Height Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error Url Not Found":  url.status_code}, status=status.HTTP_404_NOT_FOUND)

#################################################################################################################################

# height Measurment data
class HealthActivityList(APIView):

    @staticmethod
    def get(request):
        queryset = HealthActivity.objects.all()
        serializer_data = HealthActivityListSerializer(queryset, many=True)
        return Response(serializer_data.data)

    @staticmethod
    def post(request):
        try:
            with urllib.request.urlopen("http://13.232.172.131/api/user/health_activity") as url:
                response = json.loads(url.read())
                for jsondata in response:
                    updatejson = HealthActivity.objects.create(**jsondata)
                return Response({"Message": "HealthActivity Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error": "File Path allready exists"}, status=status.HTTP_404_NOT_FOUND)
#################################################################################################################################

# height Measurment data
class WeightMesurementList(APIView):

    @staticmethod
    def get(request):
        queryset = WeightMesurement.objects.all()
        serializer_data = WeightMesurementListSerializer(queryset, many=True)
        return Response(serializer_data.data)

    @staticmethod
    def post(request):
        try:
            with urllib.request.urlopen("http://13.232.172.131/api/user/weight_mesurement") as url:
                response = json.loads(url.read())
                for jsondata in response:
                    updatejson = WeightMesurement.objects.create(**jsondata)
                return Response({"Message": "WeightMesurement Data Succesfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"Error": "File Path allready exists"}, status=status.HTTP_404_NOT_FOUND)
################################################################################################################################################

class UpdateStepListView(APIView):
    @staticmethod
    def post(request, user_id):
        serializer = UpdateStepSerializer(data=request.data)
        # breakpoint()
        if serializer.is_valid(raise_exception=True):
            try:
                if validate_id(user_id):
                    user_obj = UserDetail.objects.get(_id=user_id)
                    data = serializer.validated_data
                    if UpdateStep.objects.filter(user=user_obj).last():
                        return Response({"Error": "Allready Exists"}, status=status.HTTP_400_BAD_REQUEST)
                    obj = UpdateStep.objects.create(user=user_obj, step=data.get("step"), date=data.get("date"),
                                                    time=data.get("time"), calories=data.get("calories"),
                                                    speed_avg=data.get("speed_avg"),
                                                    distance=data.get("distance"))

                    return Response({"data": "data is insert"}, status=status.HTTP_200_OK)
                return Response({"Error": "Invalid user id"}, status=status.HTTP_400_BAD_REQUEST)
            except UserDetail.DoesNotExist:

                return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_404_NOT_FOUND)

    @staticmethod
    def get(request, user_id):
        try:
            # breakpoint() 
            user = UserDetail.objects.get(_id=user_id)
            user_obj = UpdateStep.objects.get(user=user)

            serializer = UpdateStepSerializer(user_obj)
            return Response(serializer.data)
        except:
            return Response({"Error": "File Path does not exists"}, status=status.HTTP_404_NOT_FOUND)
################################################################################################################################################

class UpdateStepDetailView(APIView):
    @staticmethod
    def patch(request, user_id):
        serializer = UpdateStepUpdateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            try:
                user = UserDetail.objects.get(_id=user_id)
                user_obj = UpdateStep.objects.get(user=user, date=date.today())
                step = user_obj.step + serializer.validated_data["step"]
                calories = user_obj.step + serializer.validated_data["calories"]
                speed_avg = user_obj.step + serializer.validated_data["speed_avg"]
                distance = user_obj.step + serializer.validated_data["distance"]
                user_obj.step = step  # user_obj.step + serializer.validated_data["step"]
                user_obj.calories = calories
                user_obj.speed_avg = speed_avg
                user_obj.distance = distance
                user_obj.save()
                return Response({"Message": "Detail update successfully."})
            except UserDetail.DoesNotExist:
                return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_404_NOT_FOUND)
            except UpdateStep.DoesNotExist:
                return Response({"Error": "User has no record in current date."}, status=status.HTTP_404_NOT_FOUND)
                
#################################################################################################################################

class UpdateSleepGoalView(APIView):
    @staticmethod
    def post(request, user_id):
        serializer = UpdateSleepGoalSerializer(data=request.data)
        # breakpoint()
        if serializer.is_valid(raise_exception=True):
            try:
                if validate_id(user_id):
                    user_obj = UserDetail.objects.get(_id=user_id)
                    data = serializer.validated_data
                    if UpdateSleepGoal.objects.filter(user=user_obj).last():
                        return Response({"Error": "Allready Exists"}, status=status.HTTP_400_BAD_REQUEST)
                    obj = UpdateSleepGoal.objects.create(user=user_obj, sleep_time=data.get("sleep_time"),
                                                         wake_time=data.get("wake_time"))

                    return Response({"data": "data is insert"}, status=status.HTTP_200_OK)
                return Response({"Error": "Invalid user id"}, status=status.HTTP_400_BAD_REQUEST)
            except UserDetail.DoesNotExist:

                return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_404_NOT_FOUND)

    @staticmethod
    def get(request, user_id):
        try:
            # breakpoint() 
            user = UserDetail.objects.get(_id=user_id)
            user_obj = UpdateSleepGoal.objects.get(user=user)

            serializer = UpdateSleepGoalSerializer(user_obj)
            return Response(serializer.data)
        except:
            return Response({"Error": "File Path does not exists"}, status=status.HTTP_404_NOT_FOUND)
################################################################################################################################################

class UpdateSleepView(APIView):
    @staticmethod
    def post(request, user_id):
        serializer = UpdateSleepSerializer(data=request.data)
        # breakpoint()
        if serializer.is_valid(raise_exception=True):
            try:
                if validate_id(user_id):
                    user_obj = UserDetail.objects.get(_id=user_id)
                    data = serializer.validated_data
                    if UpdateSleep.objects.filter(user=user_obj).last():
                        return Response({"Error": "Allready Exists"}, status=status.HTTP_400_BAD_REQUEST)
                    obj = UpdateSleep.objects.create(user=user_obj, date=data.get("date"),
                                                     sleep_time=data.get("sleep_time"), wake_time=data.get("wake_time"))

                    return Response({"data": "data is insert"}, status=status.HTTP_200_OK)
                return Response({"Error": "Invalid user id"}, status=status.HTTP_400_BAD_REQUEST)
            except UserDetail.DoesNotExist:

                return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_404_NOT_FOUND)

    @staticmethod
    def get(request, user_id):
        try:
            # breakpoint() 
            user = UserDetail.objects.get(_id=user_id)
            user_obj = UpdateSleep.objects.get(user=user)

            serializer = UpdateSleepSerializer(user_obj)
            return Response(serializer.data)
        except:
            return Response({"Error": "File Path does not exists"}, status=status.HTTP_404_NOT_FOUND)
################################################################################################################################################

class UpdateWaterIntakelView(APIView):
    @staticmethod
    def post(request, user_id):
        serializer = UpdateWaterIntakeSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            try:
                if validate_id(user_id):
                    user_obj = UserDetail.objects.get(_id=user_id)
                    data = serializer.validated_data
                    if UpdateWaterIntake.objects.filter(user=user_obj).last():
                        return Response({"Error": "Allready Exists"}, status=status.HTTP_400_BAD_REQUEST)
                    obj = UpdateWaterIntake.objects.create(user=user_obj, water_intake=data.get("water_intake"))

                    return Response({"data": "data is insert"}, status=status.HTTP_200_OK)
                return Response({"Error": "Invalid user id"}, status=status.HTTP_400_BAD_REQUEST)
            except UserDetail.DoesNotExist:

                return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_404_NOT_FOUND)

    @staticmethod
    def get(request, user_id):
        try:
            # breakpoint() 
            user = UserDetail.objects.get(_id=user_id)
            user_obj = UpdateWaterIntake.objects.get(user=user)

            serializer = UpdateWaterIntakeSerializer(user_obj)
            return Response(serializer.data)
        except:
            return Response({"Error": "File Path does not exists"}, status=status.HTTP_404_NOT_FOUND)
################################################################################################################################################

class AddWaterWaterIntakeView(APIView):
    @staticmethod
    def post(request, user_id):
        serializer = AddWaterIntakeSerializer(data=request.data)
        # breakpoint()
        if serializer.is_valid(raise_exception=True):
            try:
                if validate_id(user_id):
                    user_obj = UserDetail.objects.get(_id=user_id)
                    data = serializer.validated_data
                    if AddWaterIntake.objects.filter(user=user_obj).last():
                        return Response({"Error": "Allready Exists"}, status=status.HTTP_400_BAD_REQUEST)
                    obj = AddWaterIntake.objects.create(user=user_obj, date=data.get("date"),
                                                        water_intake=data.get("water_intake"))

                    return Response({"data": "data is insert"}, status=status.HTTP_200_OK)
                return Response({"Error": "Invalid user id"}, status=status.HTTP_400_BAD_REQUEST)
            except UserDetail.DoesNotExist:
                return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_404_NOT_FOUND)

    @staticmethod
    def get(request, user_id):
        try:
            # breakpoint() 
            user = UserDetail.objects.get(_id=user_id)
            user_obj = AddWaterIntake.objects.get(user=user)

            serializer = AddWaterIntakeSerializer(user_obj)
            return Response(serializer.data)
        except:
            return Response({"Error": "File Path does not exists"}, status=status.HTTP_404_NOT_FOUND)
################################################################################################################################################

class AddHeartRateView(APIView):
    @staticmethod
    def post(request, user_id):
        serializer = AddHeartRateSerializer(data=request.data)
        # breakpoint()
        if serializer.is_valid(raise_exception=True):
            try:
                if validate_id(user_id):
                    user_obj = UserDetail.objects.get(_id=user_id)
                    data = serializer.validated_data
                    if AddHeartRate.objects.filter(user=user_obj).last():
                        return Response({"Error": "Allready Exists"}, status=status.HTTP_400_BAD_REQUEST)
                    obj = AddHeartRate.objects.create(user=user_obj, date=data.get("date"), bpm=data.get("bpm"))

                    return Response({"data": "data is insert"}, status=status.HTTP_200_OK)
                return Response({"Error": "Invalid user id"}, status=status.HTTP_400_BAD_REQUEST)
            except UserDetail.DoesNotExist:
                return Response({"Error": f"User id {user_id} Does not exists."}, status=status.HTTP_404_NOT_FOUND)

    @staticmethod
    def get(request, user_id):
        try:
            # breakpoint() 
            user = UserDetail.objects.get(_id=user_id)
            user_obj = AddHeartRate.objects.get(user=user)

            serializer = AddHeartRateSerializer(user_obj)
            return Response(serializer.data)
        except:
            return Response({"Error": "File Path does not exists"}, status=status.HTTP_404_NOT_FOUND)
################################################################################################################################################






















