#!/usr/bin/env python
# coding: utf-8




# '''
# BMI: {height : "cm"(float), weight : "kg"(float)}    --> returns: "float"
# 
# IBW: {height : "cm"(float)}   --> returns: "float"
# 
# RMR: {height : "cm"(float), weight : "kg"(float), age : "years"(int), gender: { Male : 0, Female : 1}}   --> returns: "float"
# 
# req_cal: {rmr: FLOAT, activity : INT, fit_goal: INT, diff_level: FLOAT }  ---> returns: "str" or "ERROR_MESSAGE"
# 
# TBW: {height : "cm"(float), weight : "kg"(float)}    --> returns: "float"
# 
# water_intake: {weight: float}  ---> returns: "FLOAT"
# 
# body_fat: {(weight, waist, wrist, hip, forearm) --> "Float",  gender ---> {'Male:0', 'Feamle:1'}}    --> returns: "float"
# 
# muscle_fat: {body_fat:"float", weight:"float"}     --> returns: "float"
# 
# '''



def BMI(weight, height):
    '''
      Calculates the Body Mass Index (BMI) using height and weight
      and accepts 
      height in "cm",
      weight in "kg".
      
    '''
    
    return (weight*100*100)/(height*height)



def IBW(height):
    '''
    Calculates the Ideal Body Weight (IBW) using height of the person
    and accepts height in "cm".

    '''
    return height-100


def RMR(weight, height, age, gender):
    ''' 
      Calculates the Resting Metabolic Rate (RMR)

      Accepts values with unit:
      weight in "kg"
      height in "cm"
      age in "years"
      gender: { Male : 0, Female : 1} 

    '''

    if gender == 0:
        val =  (10*weight + 6.25*height - 5*age + 5)
        return val
    

    else:
        val = (10*weight + 6.25*height - 5*age - 161)
        return val




def req_cal(rmr, activity, fit_goal, diff_level, gender):
    '''
      Function to calculate the recommended calories for the user according to its activity nature and gender.

      Accepts values with unit:
      rmr means Resting Metabolic Rate as "kcal"
      activity means activity level as { Sedentary:0, Light:1, Intense:2 }
      fit_goal means fitness goal as { Weight Lose:0, Muscles Gain:1, Stay in Shape:2 }
      gender: { Male : 0, Female : 1 }
      diff_level means difficulty level as 
      "Easy"       --> (0.4 - 0.7)kg/week , 
      "Moderate"   --> (0.75 - 1)kg/week, 
      "Difficult"  --> (1.2 - 1.5)kg/week
      
      
    '''
    if activity == 0:
        if fit_goal == 0: 
            if gender == 0:
                act_bmr = 1.6*rmr
                kcal = act_bmr - (diff_level*1000)
            else:
                act_bmr = 1.5*rmr
                kcal = act_bmr - (diff_level*1000)
          
        elif fit_goal == 1:
            if gender == 0:
                act_bmr = 1.6*rmr
                kcal = act_bmr + (diff_level*1000)
            else:
                act_bmr = 1.5*rmr
                kcal = act_bmr + (diff_level*1000)

        else:
            if gender == 0:
                kcal = 1.6*rmr
            else:
                kcal = 1.5*rmr

    elif activity == 1:
        if fit_goal == 0: 
            if gender == 0:
                act_bmr = 1.72*rmr
                kcal = act_bmr - (diff_level*1000)
            else:
                act_bmr = 1.68*rmr
                kcal = act_bmr - (diff_level*1000)

        elif fit_goal == 1:
            if gender == 0:
                act_bmr = 1.72*rmr
                kcal = act_bmr + (diff_level*1000)
            else:
                act_bmr = 1.68*rmr
                kcal = act_bmr + (diff_level*1000)

        else:
            if gender == 0:
                kcal = 1.72*rmr
            else:
                kcal = 1.68*rmr

    elif activity == 2:
        if fit_goal == 0: 
            if gender == 0:
                act_bmr = 1.9*rmr
                kcal = act_bmr - (diff_level*1000)
            else:
                act_bmr = 1.8*rmr
                kcal = act_bmr - (diff_level*1000)

        elif fit_goal == 1:
            if gender == 0:
                act_bmr = 1.9*rmr
                kcal = act_bmr + (diff_level*1000)
            else:
                act_bmr = 1.8*rmr
                kcal = act_bmr + (diff_level*1000)

        else:
            if gender == 0:
                kcal = 1.9*rmr
            else:
                kcal = 1.8*rmr
    try:
        if kcal > rmr:
            return str(kcal)
        else:
            raise Exception()
    except:
        return ('The fitness goal chosen by you looks dangerous for your body. Please decrease by some amount.{Null}')
    



def TBW(height, weight):
    '''
      Total Body Water: Shows the volume of water present in the body.
      minimum water percentage in a body is
      60%  for Male
      57%  for Female

      Accepts values as:
      height in "cm"
      weight in "kg"
    '''
    val = ((0.194786*height + 0.296785*weight - 14.012934)/(weight))*100
    return val



def water_intake(weight):
    '''
    It returns the number of glasses required for the user.
    1 glass = 250ml of water 
    '''
    import math as m
    
    water = weight/30
    glass = m.ceil(water/0.25)
    return glass




def body_fat(weight, waist, gender, wrist=0, hip=0, forearm=0):
    '''
    This function calculates the body fat percentage, and accepts values as:
    weight ---> "kg"
    waist  ---> "cm"
    wrist  ---> "cm"
    hip    ---> "cm"
    forearm---> "cm"
    gender ---> {'Male:0', 'Feamle:1'}
    '''
    
    weight = weight*2.20462
    waist = waist*0.39370
    wrist = wrist*0.39370
    hip = hip*0.39370
    forearm = forearm*0.39370
    
    if gender == 0:
        
        fat_per = ((weight- ((weight*1.082) + 94.42 - (waist * 4.15))) * 100)/weight
    
    else:
        
        fat_per = ((weight -((weight * 0.732)+ 8.987 + (wrist/3.140) - (waist*0.157) - (hip*0.2490) + (forearm*0.434)))*100)/weight

    return fat_per



def muscle_fat(fat_per, weight):
    '''
    This function is used to calculate the muscle fat percentage.
    fat_per  --> fat percentage of the user as "float".
    weight  ---> in "kg"
    '''
    
    fat = (fat_per*weight)/100
    muscle = ((weight - fat)*100)/weight
    
    return muscle





# weight = 87
# height = 181
# age = 33
# gender = 0
# activity = 0
# fit_goal = 0
# diff_level = 0.5
# waist = 97

# bmi = BMI(weight, height)
# print('BMI value: ', bmi)

# ibw = IBW(height)
# print('Ideal Body Weight is: ',ibw)

# rmr = RMR(weight, height, age, gender)
# print('Resting metabolic rate is: ',rmr)

# cal = req_cal(rmr, activity, fit_goal, diff_level, gender)
# print('recommended calorie is: ', cal)

# body_water = TBW(height, weight)
# print('Total body water percentage is: ', body_water)

# glass = water_intake(weight)
# print('Number of glasses of water required for the user is: ',glass)

# bd_fat = body_fat(weight, waist, gender)
# print('Body fat percentage is: ',bd_fat)

# mus_fat = muscle_fat(bd_fat, weight)
# print('muscle fat percentage is: ', mus_fat)




