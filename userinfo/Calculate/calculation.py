#!/usr/bin/env python
# coding: utf-8

##### 1

def BMI(weight, height):
    '''
      Calculates the Body Mass Index (BMI) using height and weight
      and accepts 
      height in "cm",
      weight in "kg".
      
    '''
    
    return (weight*100*100)/(height*height)


### 2


def IBW(height):
    '''
    Calculates the Ideal Body Weight (IBW) using height of the person
    and accepts height in "cm".

    '''
    return height-100

## 3 


def RMR(weight, height, age, gender):
    ''' 
      Calculates the Resting Metabolic Rate (RMR)

      Accepts values with unit:
      weight in "kg"
      height in "cm"
      age in "years"

    '''

    if gender in ['Male','male', 'm']:
        val =  (10*weight + 6.25*height - 5*age + 5)
        return val
    

    else:
        val = (10*weight + 6.25*height - 5*age - 161)
        return val

#### 4


def req_cal(rmr, activity, fit_goal, diff_level, gender):
    '''
      Function to calculate the recommended calories for the user according to its activity nature and gender.

      Accepts values with unit:
      rmr means Resting Metabolic Rate as "kcal"
      activity means activity level as "Sedentary", "Light", "Intense"
      fit_goal means fitness goal as "Weight Lose", "Muscles Gain", "Stay in Shape"

      diff_level means difficulty level as 
      "Easy"       --> (0.4 - 0.7)kg/week , 
      "Moderate"   --> (0.75 - 1)kg/week, 
      "Difficult"  --> (1.2 - 1.5)kg/week

    '''
    if activity in ['Sedentary']:
        if fit_goal in ['Weight Lose']: 
            if gender in ['Male','male', 'm']:
                act_bmr = 1.6*rmr
                kcal = act_bmr - (diff_level*1000)
                if kcal < rmr:
                    return 'The fitness goal chosen by you looks dangerous for your body. Please decrease by some amount.'
            else:
                act_bmr = 1.5*rmr
                kcal = act_bmr - (diff_level*1000)
          
        elif fit_goal in ['Muscles Gain']:
            if gender in ['Male','male', 'm']:
                act_bmr = 1.6*rmr
                kcal = act_bmr + (diff_level*1000)
            else:
                act_bmr = 1.5*rmr
                kcal = act_bmr + (diff_level*1000)

        else:
            if gender in ['Male','male', 'm']:
                kcal = 1.6*rmr
            else:
                kcal = 1.5*rmr

    elif activity in ['Light']:
        if fit_goal in ['Weight Lose']: 
            if gender in ['Male','male', 'm']:
                act_bmr = 1.72*rmr
                kcal = act_bmr - (diff_level*1000)
                if kcal < rmr:
                    return 'The fitness goal chosen by you looks dangerous for your body. Please decrease by some amount.'
            else:
                act_bmr = 1.68*rmr
                kcal = act_bmr - (diff_level*1000)

        elif fit_goal in ['Muscles Gain']:
            if gender in ['Male','male', 'm']:
                act_bmr = 1.72*rmr
                kcal = act_bmr + (diff_level*1000)
            else:
                act_bmr = 1.68*rmr
                kcal = act_bmr + (diff_level*1000)

        else:
            if gender in ['Male','male', 'm']:
                kcal = 1.72*rmr
            else:
                kcal = 1.68*rmr

    elif activity in ['Intense']:
        if fit_goal in ['Weight Lose']: 
            if gender in ['Male','male', 'm']:
                act_bmr = 1.9*rmr
                kcal = act_bmr - (diff_level*1000)
                if kcal < rmr:
                    return 'The fitness goal chosen by you looks dangerous for your body. Please decrease by some amount.'
            else:
                act_bmr = 1.8*rmr
                kcal = act_bmr - (diff_level*1000)

        elif fit_goal in ['Muscles Gain']:
            if gender in ['Male','male', 'm']:
                act_bmr = 1.9*rmr
                kcal = act_bmr + (diff_level*1000)
            else:
                act_bmr = 1.8*rmr
                kcal = act_bmr + (diff_level*1000)

        else:
            if gender in ['Male','male', 'm']:
                kcal = 1.9*rmr
            else:
                kcal = 1.8*rmr
    
    return kcal


##### 5

def TBW(height, weight):
    '''
      Total Body Water: Shows the volume of water present in the body.
      minimum water percentage in a body is
      60%  for Male
      57%  for Female

      Accepts values as:
      height in "cm"
      weight in "kg"
    '''
    val = (0.194786*height + 0.296785*weight - 14.012934)
    return val


####### 6

def water_intake(weight):
    '''
    It returns the number of glasses required for the user.
    1 glass = 250ml of water 
    '''
    import math as m
    
    water = weight/30
    glass = m.ceil(water/0.25)
    return glass

####  7

def body_fat(weight, waist, gender, wrist, hip, forearm):
    
    if gender in ['Male', 'male', 'm']:
        
        fat_per = ((wt- ((wtx1.082) + 94.42-(waist x 4.15))) * 100)/weight
        
    else:
        
        fat_per = (WEIGHT -( (WEIGHT X 0.732)+ 8.987 + ( WRIST/3.140) - ( WAIST X 0.157) - (HIP X 0.2490) + ( FOREARM X 0.434)))
        
    return fat_per


###### 8


weight = 87
height = 181
age = 33
gender = 'Male'
activity = 'Sedentary'
fit_goal = 'Weight Lose'
diff_level = 0.5

bmi = BMI(weight, height)
print('BMI value: ', bmi)

ibw = IBW(height)
print('Ideal Body Weight is: ',ibw)

rmr = RMR(weight, height, age, gender)
print('Resting metabolic rate is: ',rmr)

cal = req_cal(rmr, activity, fit_goal, diff_level, gender)
print('recommended calorie is: ', cal)

body_water = TBW(height, weight)
print('Total body water is: ', body_water)

glass = water_intake(weight)
print('Number of glasses of water required for the user is: ',glass)

