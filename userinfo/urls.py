from django.urls import path
from .views import *


urlpatterns = [

    path('mobileregister/', RegisterUserList.as_view(), name="mobile_user_list"),
    path('update_userdetail/<str:user_id>/', UserDetailList.as_view()),
    path('health_calculation/<str:user_id>/', HealthCalculationList.as_view()),

    path('heightlist/', HeightList.as_view()),
    path('healthActivity/', HealthActivityList.as_view()),
    path('weightmesurement/', WeightMesurementList.as_view()),
   
    path('step/<str:user_id>/', UpdateStepListView.as_view()),
    path('sleepgoal/<str:user_id>/', UpdateSleepGoalView.as_view()),
    path('updatesleep/<str:user_id>/', UpdateSleepView.as_view()),
    path('waterintake/<str:user_id>/', UpdateWaterIntakelView.as_view()),
    path('addwaterintake/<str:user_id>/', AddWaterWaterIntakeView.as_view()),
    path('addheartrate/<str:user_id>/', AddHeartRateView.as_view()),
    path('updateStep/<str:user_id>/', UpdateStepDetailView.as_view(), name="update_step"),

]

