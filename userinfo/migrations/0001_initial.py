# Generated by Django 2.2.19 on 2021-04-21 12:23

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import djongo.models.fields
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RegisterUser',
            fields=[
                ('_id', djongo.models.fields.ObjectIdField(auto_created=True, primary_key=True, serialize=False)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text='A unique number to identify the user')),
                ('phone_number', models.CharField(max_length=20, unique=True)),
                ('country_code', models.CharField(max_length=10)),
                ('device_id', models.CharField(max_length=50)),
                ('password', models.CharField(max_length=50)),
                ('is_active', models.BooleanField(default=True)),
                ('is_admin', models.BooleanField(default=False)),
                ('last_login', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='HealthActivity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to='product_images')),
            ],
        ),
        migrations.CreateModel(
            name='Height',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cm', models.CharField(max_length=50)),
                ('feet', models.CharField(max_length=50)),
                ('feet_inch', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='WeightMesurement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kg', models.CharField(max_length=50)),
                ('lbs', models.CharField(max_length=50)),
                ('ounce', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='UserDetail',
            fields=[
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='register_on_user', serialize=False, to=settings.AUTH_USER_MODEL)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text='A unique number to identify the user')),
                ('name', models.CharField(max_length=50)),
                ('gender', models.IntegerField(choices=[(0, 'Male'), (1, 'Female')])),
                ('age', models.IntegerField()),
                ('waist', models.IntegerField()),
                ('current_weight', models.FloatField()),
                ('height', models.FloatField()),
                ('target_weight', models.FloatField()),
                ('diffcult_level', models.FloatField()),
                ('fitness_goals', models.IntegerField()),
                ('activity', models.IntegerField()),
                ('height_unit', models.IntegerField(choices=[(0, 'Fit/Inch'), (1, 'CM')])),
                ('weight_unit', models.IntegerField(choices=[(0, 'KG'), (1, 'LBS')])),
                ('paymentverify', models.CharField(max_length=50)),
                ('verifystatus', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='HealthCalculation',
            fields=[
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='result_on_user', serialize=False, to='userinfo.UserDetail')),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, help_text='A unique number to identify the user')),
                ('bmi', models.FloatField()),
                ('bmr', models.FloatField()),
                ('ibw', models.FloatField()),
                ('water', models.FloatField()),
                ('total_body_water', models.FloatField()),
                ('your_target', models.CharField(max_length=50)),
                ('calorie', models.CharField(max_length=50)),
                ('fat', models.FloatField()),
                ('muscles_fat', models.FloatField()),
                ('fitness_level', models.CharField(blank=True, max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='UpdateWaterIntake',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('water_intake', models.IntegerField()),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='userinfo.UserDetail')),
            ],
        ),
        migrations.CreateModel(
            name='UpdateStep',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('step', models.CharField(max_length=250)),
                ('time', models.TimeField()),
                ('date', models.DateField()),
                ('calories', models.CharField(max_length=250)),
                ('speed_avg', models.IntegerField()),
                ('distance', models.IntegerField()),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='userinfo.UserDetail')),
            ],
        ),
        migrations.CreateModel(
            name='UpdateSleepGoal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sleep_time', models.CharField(max_length=50)),
                ('wake_time', models.CharField(max_length=50)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='userinfo.UserDetail')),
            ],
        ),
        migrations.CreateModel(
            name='UpdateSleep',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('sleep_time', models.CharField(max_length=50)),
                ('wake_time', models.CharField(max_length=50)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='userinfo.UserDetail')),
            ],
        ),
        migrations.CreateModel(
            name='AddWaterIntake',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('water_intake', models.IntegerField()),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='userinfo.UserDetail')),
            ],
        ),
        migrations.CreateModel(
            name='AddHeartRate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('bpm', models.IntegerField()),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='userinfo.UserDetail')),
            ],
        ),
    ]
