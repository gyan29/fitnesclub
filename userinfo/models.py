import uuid,urllib,os
from jsonfield import JSONField
from django.db import models
from djongo import models
# from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractBaseUser,BaseUserManager
from django.utils.translation import ugettext_lazy as _
from bson.objectid import ObjectId



class RegisterUserManager(BaseUserManager):

    def create_user(self, phone_number,password):

        if not phone_number:
            raise ValueError('The phone number must be set')

        user = self.model(
            phone_number=phone_number,
            password = password,)

        user.save(using = self._db)
       
        return user
        
    def create_superuser(self,phone_number,password, **extra_fields):

        user = self.create_user(
            phone_number,
            password = password
        )
        user.is_admin =  True
        user.save(using= self._db)

        return user



class RegisterUser(AbstractBaseUser):
     
    _id = models.ObjectIdField()
    id = models.UUIDField(default=uuid.uuid4,editable=False,help_text="A unique number to identify the user",primary_key=False)
    phone_number = models.CharField(max_length=20,unique=True) 
    country_code = models.CharField(max_length=10,unique=False) 
    device_id = models.CharField(max_length=50) 
    password = models.CharField(max_length=50)
    is_active = models.BooleanField(default = True)
    is_admin = models.BooleanField(default= False)
    last_login = models.DateTimeField(auto_now= True)


    USERNAME_FIELD = 'phone_number'
    REQUIRED_FIELDS = []

    objects = RegisterUserManager()

    def __str__(self):
        return self.phone_number

    def has_perm(self, perm, obj = None):
        return True

    def has_module_perms(self,perm,obj = None):
        return True

    @property
    def is_staff(self):
        return self.is_admin



class UserDetail(models.Model):
    GENDER_CHOICES = (
        (0, 'Male'),
        (1, 'Female'),
    )

    height_unit = (
        (0, 'Fit/Inch'),
        (1, 'CM'),
    )

    weight_unit = (
        (0, 'KG'),
        (1, 'LBS'),
    )

    
    user = models.OneToOneField(RegisterUser, on_delete=models.CASCADE,related_name="register_on_user",primary_key=True)
    id = models.UUIDField(default=uuid.uuid4,editable=False,help_text="A unique number to identify the user",primary_key=False)
    name = models.CharField(max_length=50)
    gender = models.IntegerField(choices=GENDER_CHOICES)
    age = models.IntegerField()
    waist = models.IntegerField()
    current_weight = models.FloatField()
    height = models.FloatField()
    target_weight = models.FloatField()
    diffcult_level = models.FloatField()
    fitness_goals = models.IntegerField()
    activity = models.IntegerField()
    height_unit = models.IntegerField(choices=height_unit)
    weight_unit = models.IntegerField(choices=weight_unit)
    paymentverify = models.CharField(max_length=50)
    verifystatus = models.BooleanField(default=False,)

    def __str__(self):
        return self.name


#############################################################################################

class HealthCalculation(models.Model):
    user = models.OneToOneField(UserDetail, on_delete=models.CASCADE,related_name="result_on_user",primary_key=True)
    id = models.UUIDField(default=uuid.uuid4,editable=False,help_text="A unique number to identify the user",primary_key=False)
    bmi = models.FloatField()
    bmr = models.FloatField()
    ibw = models.FloatField()
    water = models.FloatField()
    total_body_water = models.FloatField()
    your_target = models.CharField(max_length=50)
    calorie = models.CharField(max_length=50)
    fat = models.FloatField()
    muscles_fat = models.FloatField()
    fitness_level = models.CharField(max_length=100,null=True,blank=True)

    def __str__(self):
        return "{}".format(self.user)

#############################################################################################

class Height(models.Model):
    cm = models.CharField(max_length=50)
    feet = models.CharField(max_length=50)
    feet_inch = models.CharField(max_length=50)


class HealthActivity(models.Model):
    title = models.CharField(max_length=50)
    image = models.ImageField(upload_to='product_images')
    

class WeightMesurement(models.Model):
    kg = models.CharField(max_length=50)
    lbs = models.CharField(max_length=50)
    ounce = models.CharField(max_length=50)

#############################################################################################

# 1
class UpdateStep(models.Model):
    
    user = models.ForeignKey(UserDetail,on_delete=models.CASCADE)
    step = models.CharField(max_length=250)
    time = models.TimeField(auto_now=False, auto_now_add=False)
    date = models.DateField()
    calories = models.CharField(max_length=250)
    speed_avg = models.IntegerField()
    distance = models.IntegerField()

    def __str__(self):
        return self.user


#######
 # 2
class UpdateSleepGoal(models.Model):
    user = models.ForeignKey(UserDetail,on_delete=models.CASCADE)
    sleep_time = models.CharField(max_length=50)
    wake_time = models.CharField(max_length=50)


    def __str__(self):
        return self.user

#####
# 3           
class UpdateSleep(models.Model):
    user = models.ForeignKey(UserDetail,on_delete=models.CASCADE)
    date = models.DateField()
    sleep_time = models.CharField(max_length=50)
    wake_time = models.CharField(max_length=50)

    def __str__(self):
        return self.user


# # 4
class UpdateWaterIntake(models.Model):
    user = models.ForeignKey(UserDetail,on_delete=models.CASCADE)
    water_intake = models.IntegerField()

    def __str__(self):
        return self.user

####
#  5
class AddWaterIntake(models.Model):
    user = models.ForeignKey(UserDetail,on_delete=models.CASCADE)
    date = models.DateField()
    water_intake = models.IntegerField()

    def __str__(self):
        return self.user

####
# 6
class AddHeartRate(models.Model):
    user = models.ForeignKey(UserDetail,on_delete=models.CASCADE)
    date = models.DateField()
    bpm = models.IntegerField()

    def __str__(self):
        return self.user










