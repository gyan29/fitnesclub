from rest_framework import serializers, status
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from django.contrib.auth.hashers import make_password
# from rest_framework.serializers import (RegisterUserListSerializer,ValidationError)
import re
from .models import *


##############################################################################################

class LoginSerializer(TokenObtainPairSerializer):
    
    @classmethod
    def get_token(cls,user):
        token = super().get_token(user)
        token['phone_number'] = user.phone_number.raw_phone
        token['password'] = user.password
        return token

    
    def validate(self,attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)
        data['phone_number'] = self.user.phone_number
        return data
###################################################################################################

class RegisterUserListSerializer(serializers.ModelSerializer):
    password = serializers.CharField(required=False)

    class Meta:
        model = RegisterUser
        fields = ["phone_number", "device_id", "password","country_code"]

    def validate_phone_number(self, value):
        regex = r'^\+?1?\d{10,11}$'
        val = str(value)
        if re.match(regex, val):
            return (value)
        raise serializers.ValidationError("Invalid data")
    
    def validate_password(self, value: str) -> str:
        return make_password(value) 
###################################################################################################

class UserDetaillistSerializer(serializers.ModelSerializer):
    user_id = serializers.CharField(required=False)
    name = serializers.CharField(required=False)
    gender = serializers.IntegerField(required=False)
    age = serializers.IntegerField(required=False)
    height_unit = serializers.IntegerField(required=False)
    weight_unit = serializers.IntegerField(required=False)
    waist = serializers.IntegerField(required=False)
    fitness_goals = serializers.IntegerField(required=False)
    activity = serializers.IntegerField(required=False)
    current_weight = serializers.FloatField(required=False)
    height = serializers.FloatField(required=False)
    target_weight = serializers.FloatField(required=False)
    diffcult_level = serializers.FloatField(required=False)
    paymentverify = serializers.CharField(required=False)
    verifystatus  = serializers.BooleanField(required=False)

    class Meta:
        model = UserDetail
        fields = ["user_id","name", "gender", "age", 'current_weight', "height", 'target_weight', "diffcult_level",
                  'activity', 'fitness_goals', 'height_unit', 'weight_unit', 'waist', "paymentverify", "verifystatus"]
###########################################################################################################


class HealthCalculationListSerializer(serializers.ModelSerializer):
    user_id = serializers.CharField(required=False)
    class Meta:
        model = HealthCalculation
        fields = ["user_id","bmi", "bmr", "ibw", "water", "total_body_water", "your_target", "calorie", "fat", "muscles_fat"]
#########################################################################################################

class HeightListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Height
        fields = '__all__'
        # fields = ['cm', 'feet', 'feet_inch']
##########################################################################################################

class HealthActivityListSerializer(serializers.ModelSerializer):
    class Meta:
        model = HealthActivity
        fields = '__all__'
##########################################################################################################

class WeightMesurementListSerializer(serializers.ModelSerializer):
    class Meta:
        model = WeightMesurement
        fields = '__all__'
###########################################################################################################

class UpdateStepSerializer(serializers.ModelSerializer):
    class Meta:
        model = UpdateStep
        fields = ["time", "step", "date", "calories", "speed_avg", "distance"]
###########################################################################################################

class UpdateSleepGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = UpdateSleepGoal
        fields = ["sleep_time", "wake_time"]
###########################################################################################################

class UpdateSleepSerializer(serializers.ModelSerializer):
    class Meta:
        model = UpdateSleep
        fields = ["date", "sleep_time", "wake_time"]
###########################################################################################################

class UpdateWaterIntakeSerializer(serializers.ModelSerializer):
    class Meta:
        model = UpdateWaterIntake
        fields = ["water_intake"]
###########################################################################################################

class AddWaterIntakeSerializer(serializers.ModelSerializer):
    class Meta:
        model = AddWaterIntake
        fields = ["date", "water_intake"]
###########################################################################################################

class AddHeartRateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AddHeartRate
        fields = ["date", "bpm"]
###########################################################################################################

class UpdateStepUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = UpdateStep
        fields = ["step", "calories", "speed_avg", "distance"]
###########################################################################################################












    


